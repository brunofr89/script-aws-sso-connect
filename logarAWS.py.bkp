#!/usr/bin/python3

import sys
import boto.sts
import boto.s3
import requests
import getpass
import configparser
import base64
import logging
import xml.etree.ElementTree as ET
import re
from bs4 import BeautifulSoup
from os.path import expanduser
from urllib.parse import urlparse, urlunparse

##########################################################################
# Variables

sessions_acess=[]
sessions_not_acess=[]

# region: The default AWS region that this script will connect
# to for all API calls
region = 'us-east-1'

accounts=[]
accounts.append('066586941581,ipiranga-rdeai-pci')
accounts.append('965560139479,ipiranga-rdeai-anl')
accounts.append('793342191720,ipiranga-rdeai-dev')
accounts.append('921839853426,ipiranga-rdeai-esp')
accounts.append('152849375818,ipiranga-rdeai-hml')
accounts.append('978716065963,ipiranga-rdeai-int')
accounts.append('197298744561,ipiranga-rdeai-lab')
accounts.append('269363872167,ipiranga-rdeai-prd')
accounts.append('561249969270,ipiranga-rdeai-sec')
accounts.append('681951013428,ipiranga-rdeai-sys')


# output format: The AWS CLI output format that will be configured in the
# saml profile (affects subsequent CLI calls)
outputformat = 'json'

# awsconfigfile: The file where this script will store the temp
# credentials under the saml profile
awsconfigfile = '/.aws/credentials'

# SSL certificate verification: Whether or not strict certificate
# verification is done, False should only be used for dev/test
sslverification = True

# idpentryurl: The initial url that starts the authentication process.
idpentryurl = 'https://adfs.ultra.com.br/adfs/ls/IdpInitiatedSignOn?loginToRp=urn:amazon:webservices'

# Uncomment to enable low level debugging
#logging.basicConfig(level=logging.DEBUG)

##########################################################################

# Get the federated credentials from the user
print("Username: max.peixoto.ext@e-ai.com.br")
#username = input()
username = "max.peixoto.ext@e-ai.com.br"
password = getpass.getpass()
print('')

# Initiate session handler
session = requests.Session()

# Programmatically get the SAML assertion
# Opens the initial IdP url and follows all of the HTTP302 redirects, and
# gets the resulting login page
formresponse = session.get(idpentryurl, verify=sslverification)
# Capture the idpauthformsubmiturl, which is the final url after all the 302s
idpauthformsubmiturl = formresponse.url

# Parse the response and extract all the necessary values
# in order to build a dictionary of all of the form values the IdP expects
formsoup = BeautifulSoup(formresponse.text, "html.parser" )
payload = {}

for inputtag in formsoup.find_all(re.compile('(INPUT|input)')):
    name = inputtag.get('name','')
    value = inputtag.get('value','')
    if "user" in name.lower():
        #Make an educated guess that this is the right field for the username
        payload[name] = username
    elif "email" in name.lower():
        #Some IdPs also label the username field as 'email'
        payload[name] = username
    elif "pass" in name.lower():
        #Make an educated guess that this is the right field for the password
        payload[name] = password
    else:
        #Simply populate the parameter with the existing value (picks up hidden fields in the login form)
        payload[name] = value

# Debug the parameter payload if needed
# Use with caution since this will print sensitive output to the screen
#print payload

# Some IdPs don't explicitly set a form action, but if one is set we should
# build the idpauthformsubmiturl by combining the scheme and hostname 
# from the entry url with the form action target
# If the action tag doesn't exist, we just stick with the 
# idpauthformsubmiturl above
for inputtag in formsoup.find_all(re.compile('(FORM|form)')):
    action = inputtag.get('action')
    loginid = inputtag.get('id')
    if (action and loginid == "loginForm"):
        parsedurl = urlparse(idpentryurl)
        idpauthformsubmiturl = parsedurl.scheme + "://" + parsedurl.netloc + action

# Performs the submission of the IdP login form with the above post data
response = session.post(
    idpauthformsubmiturl, data=payload, verify=sslverification)

# Debug the response if needed
#print (response.text)

# Overwrite and delete the credential variables, just for safety
username = '##############################################'
password = '##############################################'
del username
del password

# Decode the response and extract the SAML assertion
soup = BeautifulSoup(response.text, "html.parser" )
assertion = ''

# Look for the SAMLResponse attribute of the input tag (determined by
# analyzing the debug print lines above)
for inputtag in soup.find_all('input'):
    if(inputtag.get('name') == 'SAMLResponse'):
        #print(inputtag.get('value'))
        assertion = inputtag.get('value')

# Better error handling is required for production use.
if (assertion == ''):
    #TODO: Insert valid error checking/handling
    print('Response did not contain a valid SAML assertion')
    sys.exit(0)

# Debug only
# print(base64.b64decode(assertion))

# Parse the returned assertion and extract the authorized roles
print('Aguarde, autenticando em todos os ambientes...')
awsroles = []
root = ET.fromstring(base64.b64decode(assertion))
for saml2attribute in root.iter('{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'):
    if (saml2attribute.get('Name') == 'https://aws.amazon.com/SAML/Attributes/Role'):
        for saml2attributevalue in saml2attribute.iter('{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue'):
            awsroles.append(saml2attributevalue.text)

# Note the format of the attribute value should be role_arn,principal_arn
# but lots of blogs list it as principal_arn,role_arn so let's reverse
# them if needed
for awsrole in awsroles:
    chunks = awsrole.split(',')
    if'saml-provider' in chunks[0]:
        profile=''
        for account in accounts:
            accountid=account.split(',')[0]
            accountname=account.split(',')[1]
            if (chunks[1].split(':')[4] == accountid):
                profile=accountname
        if (profile != ''):
            newawsrole = chunks[1] + ',' + chunks[0]+ ',' + profile
            index = awsroles.index(awsrole)
            awsroles.insert(index, newawsrole)
            awsroles.remove(awsrole)

i = 0
for awsrole in awsroles:
    selectedroleindex = i
    try:
        role_arn = awsroles[int(selectedroleindex)].split(',')[0]
        principal_arn = awsroles[int(selectedroleindex)].split(',')[1]
        session_name=awsroles[int(selectedroleindex)].split(',')[2]
    except IndexError as e:
        i += 1
        continue
    access_key=''
    secret_key=''
    session_token=''
    try:
        # Use the assertion to get an AWS STS token using Assume Role with SAML
        conn = boto.sts.connect_to_region(region)
        token = conn.assume_role_with_saml(role_arn, principal_arn, assertion)
         
         
         
         # Put the credentials into a saml specific section instead of clobbering
         # the default credentials
        sessions_acess.append(session_name)
        
        access_key=token.credentials.access_key
        secret_key=token.credentials.secret_key
        session_token=token.credentials.session_token
    except Exception as e:
        sessions_not_acess.append(session_name)   
    finally:
    # Write the AWS STS token into the AWS credential file
        home = expanduser("~")
        filename = home + awsconfigfile
        
        # Read in the existing config file
        config = configparser.RawConfigParser()
        config.read(filename)
        if not config.has_section(session_name):
           config.add_section(session_name)
                 
        config.set(session_name, 'output', outputformat)
        config.set(session_name, 'region', region)
        config.set(session_name, 'aws_access_key_id', access_key)
        config.set(session_name, 'aws_secret_access_key', secret_key)
        config.set(session_name, 'aws_session_token', session_token)
        with open(filename, 'w+') as configfile:
            config.write(configfile)
        i += 1
# Give the user some basic info as to what has just happened
print('\n\n----------------------------------------------------------------')
if sessions_not_acess:
    print('Voce nao tem permissao para os profiles:')
    for session_name in sessions_not_acess:
            print(session_name)
if sessions_acess:
     print('Foi criada nova chave de acesso em file {0} para os profiles:'.format(filename))
     for session_name in sessions_acess:
         print(session_name)
     print('Sua sessao expirara em {0}.'.format(token.credentials.expiration))
     print('Para usar as credenciais, executar o AWS CLI com a opcao --profile (e.x. aws ec2 describe-instances --profile (Dentro arquivo aws/credentials) ).')
print('----------------------------------------------------------------\n\n')
